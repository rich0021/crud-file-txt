<?php
    require_once '../service/reader.php';

    $file = new FileReader('../db.txt');

    if ($_POST['nis'] && $_POST['nama'] && $_POST['kelas']) {
        //get data
        $nis = $_POST['nis'];
        $nama = $_POST['nama'];
        $kelas = $_POST['kelas'];

        //write new data
        fwrite($file->file, $nis.','.$nama.','.$kelas."\n");

        //close reader
        fclose($file->file);

        //redirect to home
        header('location: /');
    }else{
        echo 'No Data Found';
    }
?>