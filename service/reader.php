<?php
class FileReader {
    var $file;
    var $path;

    public function __construct($path)
    {
        $this->path = $path;
        if (file_exists($path)) {
            $this->file = fopen($path, 'a+');
        }else{
            $this->file = fopen($path, 'w+');
        }
    }

    public function changeMode($mode){
        $this->file = fopen($this->path, $mode);
    }
}


