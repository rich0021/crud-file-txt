<?php

require_once '../service/reader.php';

$file = new FileReader('../db.txt');

if ($_GET['nis']) {
        $nis = htmlspecialchars($_GET['nis']);
        $data = [];

        //get current data except deleted data
        while(!feof($file->file)){
            $item = fgets($file->file);
            if(strlen($item) > 0 && explode(",", $item)[0] != $nis){
                array_push($data, trim($item));
            }
        }

        //change mode to overwrite
        $file->changeMode('w+');

        //write new data
        fwrite($file->file, implode("\n", $data)."\n");

        // close reader
        fclose($file->file);

        //redirect to home
        header('location: /');
    }else{
        echo 'No Data Found';
    }
