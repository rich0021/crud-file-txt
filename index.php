<?php
require_once 'service/reader.php';
$file = new FileReader('db.txt');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Crud File</title>
</head>
<body>
    <form action="/tambah" method="post" id="formTambah" style="margin-bottom: 30px;">
        <div>
            <label for="nis">Nis : </label>
            <input type="text" id="nis" name="nis">
        </div>
        <div>
            <label for="nama">Nama : </label>
            <input type="text" id="nama" name="nama">
        </div>
        <div>
            <label for="kelas">Kelas : </label>
            <input type="text" id="kelas" name="kelas">
        </div>
        <button style="margin-top: 10px;" type="submit">Tambah</button>
    </form>
    <form style="display:none" action="/update" method="post" id="formUpdate" style="margin-bottom: 30px;">
        <div>
            <label for="nis">Nis : </label>
            <input type="text" id="nisU" name="nis">
        </div>
        <div>
            <label for="nama">Nama : </label>
            <input type="text" id="namaU" name="nama">
        </div>
        <div>
            <label for="kelas">Kelas : </label>
            <input type="text" id="kelasU" name="kelas">
        </div>
        <input type="hidden" id="hidden_nis" name="hidden_nis">
        <button style="margin-top: 10px;" type="submit">Update</button>
        <input type="button" value="Cancel" onclick="cancelUpdate()">
    </form>
    
    <h2>Data</h2>
    <div>
        <form action="/search">
            <label for="cari">Cari Nama: </label>
            <input type="text" id="cari" name="cari">
            <button type="submit">Cari</button>
        </form>
    </div>
    <table style="margin-top: 20px;">
        <tr>
            <th>nis</th>
            <th>nama</th>
            <th>kelas</th>
            <th>aksi</th>
        </tr>
        <?php while(!feof($file->file)): ?>
            <?php $arr = explode( ',', fgets($file->file))?>
            <tr>
                <td><?= !empty($arr[0]) ? $arr[0] : ''; ?></td>
                <td><?= !empty($arr[1]) ? $arr[1] : ''; ?></td>
                <td><?= !empty($arr[2]) ? $arr[2] : ''; ?></td>
                <?php
                    if (!empty($arr[0]) || !empty($arr[1]) || !empty($arr[2])) {
                        echo "
                            <td>
                                <a href="."/hapus?nis=$arr[0]".">hapus</a> | <button data-nis='$arr[0]' data-nama='$arr[1]' data-kelas='$arr[2]' onclick='updateField(this)' class='btn-update'>Update</a>
                            </td>
                        ";
                    }
                ?>
            </tr>
        <?php endwhile; fclose($file->file)?>
    </table>

    <script>
        let formT = document.getElementById('formTambah');
        let formU = document.getElementById('formUpdate');

        function updateField(e){
            formT.style.display = 'none';
            formU.style.display = 'block';
            
            document.getElementById('namaU').value = e.dataset.nama;
            document.getElementById('nisU').value = e.dataset.nis;
            document.getElementById('kelasU').value = e.dataset.kelas;
            document.getElementById('hidden_nis').value = e.dataset.nis;
        }

        function cancelUpdate(){
            formT.style.display = 'block';
            formU.style.display = 'none';
            document.getElementById('namaU').value = "";
            document.getElementById('nisU').value = "";
            document.getElementById('kelasU').value = "";
        }
    </script>
</body>
</html>