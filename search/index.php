<?php
    require_once '../service/reader.php';

    $file = new FileReader('../db.txt');
    $search = $_GET['cari'];
    $data = [];

    //get current data except deleted data
    while(!feof($file->file)){
        $item = fgets($file->file);
        if(strlen($item) > 0 && str_contains(explode(",", $item)[1], $search) ){
            array_push($data, trim($item));
        }
    }

    fclose($file->file);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cari</title>
</head>
<body>
    <a href="/">Home</a>
    </br>
    <?php 
        if (!empty($data)) {
            echo '<h2>Data Ditemukan</h2>';
        }else{
            echo '<h2>Data Tidak Ditemukan</h2>'; 
        }
    ?>
    <table 
        <?php 
            if (!empty($data)) {
                echo 'style="margin-top: 20px;"';
            }else{
                echo 'style="margin-top: 20px; display: none"'; 
            }
        ?>>
        <tr>
            <th>nis</th>
            <th>nama</th>
            <th>kelas</th>
        </tr>
        <?php foreach($data as $item): ?>
            <?php $arr = explode(",", $item)?>
            <tr>
                <td><?= !empty($arr[0]) ? $arr[0] : ''; ?></td>
                <td><?= !empty($arr[1]) ? $arr[1] : ''; ?></td>
                <td><?= !empty($arr[2]) ? $arr[2] : ''; ?></td>
            </tr>
        <?php endforeach;?>
    </table>
</body>
</html>